
# iTunes Search Code Example

## Architecture
The architecture of this app is done in a way that can be evolved and scaled into a bigger app. I could have chosen an easier way to implement this basic functionality (a Master controller that searches on iTunes and a Details view which displays the details of each search item), but I wanted to create a skeleton that allows the app to grow. Often apps start with a small team of developers but eventually, a much bigger team ends up working on the same code. It is good if you structure the app in a way that can grow from the beginning. The app has three different layers that are totally independent of each other.

### Network layer.
This layer contains all the classes that are able to load JSON from the network. It is generic and it can load any request from the network, it is not even tided to iTunes Searches itself, this part of the code could work in any app that needs to load JSON from the network. `ISAPIClient.swift` is the main class of this layer. It defines its own request model, returns an already deserialized JSON Dictionary and it executes everything in a background thread and returns the JSON response using a block..

##### Things that can improve in this layer.
 - One of the requirements was to don't use any CocoaPods. If that was not a requirement I would have used Alamofire to take care of my network requests. This architecture allows me to do that change without modifying nothing else but the Network layer. 
  - At this point, I am not doing anything with the cache and I am relaying in `URLSession` to do that for me. In case we wanted to implement a special caching system we would have to do it in this layer. *Example: I have seen APIs that do not contain caching headers if so, we should implement our own client caching system*.

### Data layer.
In this example, this layer only has one `DataSource` since we only needed to implement one API call though, it can easily contain more classes like this one. All `DataSources`  should follow exactly the same structure:
 - Each `DataSource` defines its independent request and response. This way everything is encapsulated and this `DataSource` can work anywhere. This also allows us to unit test this class in an easy way.
 - It does not need to be a singleton since the network layer is already taking care of this.
 - No need to take care of background threading, Network layer will take care of it already.

##### Things that can improve in this layer.
 - In this example, data models as: `ISItunesItem` and the data source response: `ISItunesSearchResponse`, are tied together and defined in the same file. In case we had bigger data models we may need to separate this classes and create its own tests.
 - Serialization of the `ISItunesItem` happens within the `DataSource`. If we had more endpoints using the same data model, we should abstract this logic into a serializer class.
  - Data serialization (creation of the models) and business logic (removing the wrappers that we do not support) are in the same place. In case our business logic was much bigger, we could move all that code into a different place.

### Testing our Data Layer and Network Layer
As you can see the in the code, most of the code in the Network Layer and the Data Layer are tested by unit tests. Nothing of this code imports `UIKit` which makes testing much easier.

##### Things that can improve in this layer.
 - We should include more tests so we have a better coverage.

### UI layer
This UI layer is the only layer in the app which imports `UIKit`. It will create all the view controllers and views that we are going to use within the app. In this case, the app is really simple with just two view Controllers. 

### Architecture Diagram
Here you can see a diagram of the architecture that we just talked about. In this example, all the code lives in the same repository. IF we had a much bigger app which supports tonnes of different `DataSources`, we could easily structure all the Data and Network Layer into its own repository, create a pod out of it and have the UI App depending on that library.

![Diagrama](Resources_not_xcode/app_layers.png)