//
//  ISViewUtils.swift
//  itunesSearch-test
//
//  Created by Nicolas Yuste Tirados on 8/12/17.
//  Copyright © 2017 Nicolas Yuste. All rights reserved.
//

import UIKit

let kLoadingViewTag = 789456123

// MARK: - Some view helpers

/// Creates and returns a view with an activity indicator on it. This view will thw whole frame of it's superview.
/// It will not let the users interact with it, this way, the users will know that we are loading.
/// Also, this method automatically add this indicator view into the container View.
///
/// - Parameters:
///   - viewContainer: The view that is going to contain this indicator View
///   - startAnimate: A boolean that specifies if the activity indicator needs to be activated or not.
/// - Returns: The actual activity indicator in case you want to start it later.
func createCustomActivityIndicatory(_ viewContainer: UIView, startAnimate:Bool? = true) -> UIActivityIndicatorView {
    
    // First we need to check the loading view is not added already.
    // If so, we will not create a new and we will ad the one that is already there.
    if let loadingView = viewContainer.viewWithTag(kLoadingViewTag) {
        for subview in loadingView.subviews{
            if ((subview as? UIActivityIndicatorView) != nil){
                return subview as! UIActivityIndicatorView
            }
        }
    }
    
    let mainContainer: UIView = UIView(frame: viewContainer.frame)
    mainContainer.center = viewContainer.center
    mainContainer.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)
    mainContainer.alpha = 0.5
    mainContainer.tag = kLoadingViewTag
    mainContainer.isUserInteractionEnabled = false
    
    let viewBackgroundLoading: UIView = UIView(frame: CGRect(x:0,y: 0,width: 80,height: 80))
    viewBackgroundLoading.center = viewContainer.center
    viewBackgroundLoading.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 1)
    viewBackgroundLoading.alpha = 0.8
    viewBackgroundLoading.clipsToBounds = true
    viewBackgroundLoading.layer.cornerRadius = 15
    
    let activityIndicatorView: UIActivityIndicatorView = UIActivityIndicatorView()
    activityIndicatorView.frame = CGRect(x:0.0,y: 0.0,width: 40.0, height: 40.0)
    activityIndicatorView.activityIndicatorViewStyle =
        UIActivityIndicatorViewStyle.whiteLarge
    activityIndicatorView.center = CGPoint(x: viewBackgroundLoading.frame.size.width / 2, y: viewBackgroundLoading.frame.size.height / 2)
    viewBackgroundLoading.addSubview(activityIndicatorView)
    mainContainer.addSubview(viewBackgroundLoading)
    viewContainer.addSubview(mainContainer)
    activityIndicatorView.startAnimating()
    return activityIndicatorView
}

// MARK: - Loading images from the network


/// Loads an image from the network. It does it in a background there.
/// Once it is finished it will executing the completionHandler again in the main thread.
///
/// - Parameters:
///   - imageUrl: The url of the image that we want to download.
///   - completionHandler: The completion block that is going to be executed when finish.
func loadImageFromTheNetwork(imageUrl:String, completionHandler: @escaping (UIImage) -> Void) {
    
    let task = URLSession.shared.dataTask(with: NSURL(string: imageUrl)! as URL, completionHandler: { (data, response, error) -> Void in
        
        if error != nil {
            print("There was an error and we could not load the image: \(imageUrl). Error: \(error.debugDescription)")
            return
        }
        if let image = UIImage(data: data!) {
            DispatchQueue.main.async {
                completionHandler(image)
            }
        }
    })
    task.resume()
    
    
}

// MARK: - Item View Helper

func titleForObject(object: ISItunesItem) -> String {
    switch object.kind as ISItunesItemKind {
    case .SONG:
        return object.trackName ?? ""
    case .TV_EPISODE, .MOVIE:
        return object.artistName ?? ""
    default:
        return ""
    }
}
func subtitleForObject(object: ISItunesItem) -> String {
    switch object.kind as ISItunesItemKind {
    case .SONG:
        return object.artistName ?? ""
    case .TV_EPISODE, .MOVIE:
        return object.trackName ?? ""
    default:
        return ""
    }
}
func imageForMediaKind(kind:ISItunesItemKind) -> UIImage? {
    
    switch kind {
    case .SONG:
        return UIImage(named: "songIcon")
    case .MOVIE:
        return UIImage(named: "movieIcon")
    case .TV_EPISODE:
        return UIImage(named: "tvIcon")
    default:
        return nil // TODO: There is more assets to be included
    }
}
