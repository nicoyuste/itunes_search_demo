//
//  ISMasterViewController.swift
//  itunesSearch-test
//
//  Created by Nicolas Yuste Tirados on 8/11/17.
//  Copyright © 2017 Nicolas Yuste. All rights reserved.
//

import UIKit

class ISMasterViewController: UITableViewController, UISearchBarDelegate {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var topContainerView: UIView!
    
    var objects = [ISItunesItem]()
    var mediaTypeSearch: ISItunesSearchMediaType = .ALL

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // If you add a footerView into the tableView, it will not display empty cells
        // when its loading it's first data. This will create a white and empty space, which looks much better.
        let footerView = UIView(frame: CGRect.zero)
        self.tableView.tableFooterView = footerView
        
        #if DEBUG
            // We have this code only for DEBUG porpouses. This way, we have a populated
            // search in the screen and we do not have to type every time that we open the app.
            // DEBUG Macro will prevent this code for be executing in the Release version of the app.
            self.searchBar.text = "Lady Gaga"
            self.refreshData()
        #endif
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.refreshData()
    }
    
    // MARK: - Internal helpers for data
    
    private func refreshData() {
        
        self.refreshViewWithLoading()
        if let text = self.searchBar.text {
            let newSearch = ISItunesSearchRequest(searchTerm: text, mediaType: self.mediaTypeSearch)
            let newDataSource = ISItunesSearchDataSource(apiClient: ISAPIClient.shared)
            newDataSource.executeRequest(request: newSearch) { (response: ISItunesSearchResponse) in
                if let results = response.results, response.success {
                    self.objects = results
                    self.refreshViewWithResults()
                } else {
                    self.refreshViewWithError()
                }
            }
        } else {
            self.refreshViewWithError()
        }
    }
    
    // MARK: - Refresh View Helpers
    
    private func refreshViewWithResults() {
        self.tableView.reloadData()
        self.removeErrorView()
        self.removeLoadingView()
    }
    private func refreshViewWithError() {
        self.removeLoadingView()
        self.objects = []
        self.tableView.reloadData()
        // TODO: We need to add this view. We would do it exactly the same as the LoadingView.
    }
    private func removeErrorView() {
        // TODO: This is something that we would have to do. Just like we do with the LoadingView.
    }
    
    private func refreshViewWithLoading() {
        self.removeErrorView()
        _ = createCustomActivityIndicatory(self.view, startAnimate: true)
    }
    private func removeLoadingView() {
        for subview in self.view.subviews{
            if subview.tag == kLoadingViewTag{
                subview.removeFromSuperview()
            }
        }
    }
    
    // MARK: - Search bar / Scroll delegate
    
    @IBAction func mediaTypeChanged(_ sender: UISegmentedControl) {
        
        switch sender.selectedSegmentIndex {
            case 0: self.mediaTypeSearch = .ALL
            case 1: self.mediaTypeSearch = .MOVIE
            case 2: self.mediaTypeSearch = .MUSIC
            case 3: self.mediaTypeSearch = .TV_SHOW
            case 4: self.mediaTypeSearch = .EBOOK
            default: self.mediaTypeSearch = .ALL
        }
        self.refreshData()
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.characters.count >= 3 {
            self.refreshData()
        }
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.refreshData()
        searchBar.resignFirstResponder()
    }
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.searchBar.resignFirstResponder()
    }

    // MARK: - Segues

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            guard let hvc = segue.destination as? ISDetailViewController,
                  let item = sender as? ISItunesItem else {
                return
            }
            hvc.title = titleForObject(object: item)
            hvc.itunesItem = sender as! ISItunesItem
        }
    }

    // MARK: - Table View
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return objects.count
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "resultCell", for: indexPath) as? ISResultTableViewCell else {
            assertionFailure("Invalid cell")
            return UITableViewCell()
        }
        let object = self.objects[indexPath.row]
        
        cell.titleLabel.text = titleForObject(object: object)
        cell.subtitleLabel.text = subtitleForObject(object: object)
        cell.thumbnailImageView?.image = nil
        cell.mediaImageView?.image = imageForMediaKind(kind: object.kind)
        cell.imageViewWidthConstraint.constant = 0
        
        if let url = object.thumbnailUrl {
            loadImageFromTheNetwork(imageUrl: url, completionHandler: { (image: UIImage) in
                guard let cell = self.tableView.cellForRow(at: indexPath) as? ISResultTableViewCell else {
                    return
                }
                cell.thumbnailImageView?.image = image
                cell.imageViewWidthConstraint.constant = image.size.width
            })
        }
        return cell
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
        self.performSegue(withIdentifier: "showDetail", sender: self.objects[indexPath.row])
    }
    
}

