//
//  ISResultTableViewCell.swift
//  itunesSearch-test
//
//  Created by Nicolas Yuste on 8/11/17.
//  Copyright © 2017 Nicolas Yuste. All rights reserved.
//

import UIKit

class ISResultTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var thumbnailImageView: UIImageView!
    @IBOutlet weak var mediaImageView: UIImageView!
    @IBOutlet weak var imageViewWidthConstraint: NSLayoutConstraint!
    
}
