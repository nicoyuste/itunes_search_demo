//
//  ISDetailViewController.swift
//  itunesSearch-test
//
//  Created by Nicolas Yuste Tirados on 8/11/17.
//  Copyright © 2017 Nicolas Yuste. All rights reserved.
//

import UIKit

class ISDetailViewController: UIViewController {

    var itunesItem : ISItunesItem!
    @IBOutlet weak var artworkImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var releaseDateLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    let priceFormatter = NumberFormatter()
    let dateFormatter = DateFormatter()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.priceFormatter.numberStyle = .currency
        self.dateFormatter.dateFormat = "EEEE, MMM d, yyyy"
        
        self.configureView()
        self.fillViewWithItunesItem()
    }
    
    // MARK: - Configure the View
    
    private func configureView() {
        self.priceLabel.clipsToBounds = true
        self.priceLabel.layer.borderColor = UIColor(red:0.44, green:0.74, blue:0.35, alpha:1.00).cgColor
        self.priceLabel.layer.borderWidth = 2
        self.priceLabel.layer.cornerRadius = 5
        
        self.artworkImageView.layer.cornerRadius = 50
        self.artworkImageView.layer.masksToBounds = true
        
    }
    private func fillViewWithItunesItem(){
        
        guard self.itunesItem != nil else {
            return
        }
        
        self.titleLabel.text = titleForObject(object: self.itunesItem)
        self.subtitleLabel.text = subtitleForObject(object: self.itunesItem)
        self.descriptionLabel.text = self.itunesItem.longDescription
        
        if let url = self.itunesItem.thumbnailUrl {
            loadImageFromTheNetwork(imageUrl: url, completionHandler: { (image: UIImage) in
                self.artworkImageView.image = image
            })
        }
        
        if let price = self.itunesItem.price {
            self.priceFormatter.currencySymbol = ISPrice.getSymbolForCurrencyCode(code: price.currencyCode)
            self.priceLabel.text = self.priceFormatter.string(from: NSNumber(value: price.amount))
        }
        
        if let date = self.itunesItem.releaseDate {
            self.releaseDateLabel.text = self.dateFormatter.string(from: date)
        }
    }
    
}

