//
//  MainFlowUITests.swift
//  itunesSearch-test
//
//  Created by Nicolas Yuste Tirados on 8/13/17.
//  Copyright © 2017 Nicolas Yuste. All rights reserved.
//

import XCTest

class ISMainFlowUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        continueAfterFailure = false
        
        XCUIApplication().launch()
        
        sleep(2) // We need to wait for network data to come back
        // TODO: This is not the best way of getting real data into the app.
        // We should be able to mock the data that we populate into the tableView
        // This way we have total control over what we can click.
        
        XCUIDevice.shared().orientation = .portrait
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testOpenFirstItemNavigateToDetailsPage() {
        let app = XCUIApplication()
        
        app.tables.staticTexts["Million Reasons"].tap()
        app.navigationBars["Lady Gaga"].buttons["Master"].tap()
    }
    
    func testClickItemAfterRotation() {
        let app = XCUIApplication()
        
        XCUIDevice.shared().orientation = .landscapeLeft
        sleep(2)
        XCUIDevice.shared().orientation = .portrait
        app.tables.staticTexts["Million Reasons"].tap()
        app.navigationBars["Lady Gaga"].buttons["Master"].tap()
    }
    
    // TODO: We should create more UI tests.
    
}
