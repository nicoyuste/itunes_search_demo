//
//  ISItunesSearchResponse.swift
//  itunesSearch-test
//
//  Created by Nicolas Yuste Tirados on 8/12/17.
//  Copyright © 2017 Nicolas Yuste. All rights reserved.
//

import Foundation


/// This class encapsulates the response from the ISItunesSearch data source.
/// This class contains everything which the data source could return. 'success' will be true in case the 
/// API request was successfull, it will be false otherwise
class ISItunesSearchResponse: NSObject {
    
    let success : Bool!
    var error: Error?
    var results: [ISItunesItem]?
    init(success: Bool){
        self.success = success
    }
    
}


/// This enum encapsulates all the different items that we can get from the the iTunes Search response.
/// Each item will have a wrapper type and fields will vary depending on the wrapper.
/// WE ONLY SUPPORT 'track' wrappers at the moment
///
/// - TRACK: A wrapper type for the iTunes Search Response.
enum ISItunesItemWrapperType : String {
    case TRACK = "track"
    // case COLLECTION = "collection"
    // case ARTIST_FOR = "artistFor"
}


/// This enum enxapsulates all the different kinds of items that we an get in the iTunes Search response.
/// Each item will have a wrapper type and fields will vary depending on the wrapper.
///
/// - MOVIE: If an item is MOVIE kind, it will contain a movie in the other fields.
/// - TV_EPISODE: If an item is TV_EPISODE kind, it will contain a tv show in the other fields.
/// - SONG: If an item is SONG kind, it will contain a song in the other fields.
/// - ALBUM: If an item is ALBUM kind, it will contain an album in the other fields.
/// - BOOK: If an item is BOOK kind, it will contain a book in the other fields.
enum ISItunesItemKind : String {
    case MOVIE = "feature-movie"
    case TV_EPISODE = "tv-episode"
    case SONG = "song"
    case ALBUM = "album"
    case BOOK = "book"
}


/// This object represents an item in the iTunes Search Response.
/// There is different information that you can get out this objects: name, artist name, image, etc...
class ISItunesItem: NSObject {
    
    var wrapperType: ISItunesItemWrapperType!
    var kind : ISItunesItemKind!
    
    var thumbnailUrl : String?
    
    var trackName: String?
    var artistName: String?
    var price: ISPrice?
    var releaseDate: Date?
    var longDescription: String?
    
    init(wrapperType: ISItunesItemWrapperType, kind: ISItunesItemKind){
        self.wrapperType = wrapperType
        self.kind = kind
    }
}

/// This object represents the price of an item in the ITunes Search response.
/// Every price has both an amount and a currency code.
class ISPrice: NSObject {
    var amount: Float!
    var currencyCode: String!
    
    init(amount: Float, currencyCode:String){
        self.amount = amount
        self.currencyCode = currencyCode
    }
    
    /// Uses the system library to determinate which is the symbol for a given currencyCode.
    /// For example, for currencyCode = USD, it will return '$'
    ///
    /// - Parameter code: The currency code we want the symbol for.
    /// - Returns: The actual symbol for that currency code.
    class func getSymbolForCurrencyCode(code: String) -> String? {
        let locale = NSLocale(localeIdentifier: code)
        return locale.displayName(forKey: NSLocale.Key.currencySymbol, value: code)
    }
}
