//
//  ISItunesSearchDataSource.swift
//  itunesSearch-test
//
//  Created by Nicolas Yuste Tirados on 8/11/17.
//  Copyright © 2017 Nicolas Yuste. All rights reserved.
//

import Foundation


/// Completion block type that ISItunesSearchDataSource fires when it finishs executing a request.
typealias ISItunesSearchDataSourceCompletionBlock = (ISItunesSearchResponse) -> Void

/// This class is able to perform a search into iTunes Search.
/// It will perform the API call asynchronously and it will call back into the completion block when finished.
class ISItunesSearchDataSource: NSObject {

    private let apiClient : ISAPIClientProtocol!
    private let dateFormatter: DateFormatter = DateFormatter()
    
    /// Creates and returns a new Data Source object that is able to perform a search into iTunes Search.
    /// It requires an APIClient object in order to work. (We do this for testing porpouse so we can mock all the dependencies
    /// this class may have)
    ///
    /// - Parameter apiClient: The apiClient that is going to be able to execute the request.
    init(apiClient: ISAPIClientProtocol) {
        self.apiClient = apiClient
        self.dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
    }
    
    /// It executes a request. It will fire the completion block when it finishs.
    /// If the request does not specify and specific media, ALL will be applied by default.
    ///
    /// - Parameters:
    ///   - request: The request object that encapsulates all the request information
    ///   - completion: The completion that is going to be executed with finished.
    public func executeRequest(request: ISItunesSearchRequest, completion: @escaping ISItunesSearchDataSourceCompletionBlock)  {
        
        apiClient.executeRequest(request: request, successBlock: { (response: Dictionary<String, Any>) in
            
            guard let results = self.createResultsArrayWith(response: response) else {
                completion(ISItunesSearchResponse(success: false))
                return
            }
            
            let resultRespose = ISItunesSearchResponse(success: true)
            resultRespose.results = results
            completion(resultRespose)
            
        }) { (error: Error) in
            completion(ISItunesSearchResponse(success: false))
        }
    }
    
    // MARK: - Internal helpers
    
    private func createResultsArrayWith(response: Dictionary<String, Any>) -> [ISItunesItem]? {
        
        guard let resultsDic = response["results"] as? [[String:Any]] else {
            return nil
        }
        
        var results : [ISItunesItem] = []
        for element in resultsDic {
            if let newResult = createObjectWith(element: element) {
                results.append(newResult)
            }
        }
        
        return results.count < 1 ? nil : results
    }
    private func createObjectWith(element: [String:Any]) -> ISItunesItem? {
        
        guard
            let wrapperName = element["wrapperType"] as? String,
            let wrapperType = wrapperTypeFor(wrapperName: wrapperName),
            let kindName = element["kind"] as? String,
            let kind = kindFor(kindName: kindName) else {
                return nil
        }
        
        let result = ISItunesItem(wrapperType: wrapperType, kind: kind)
        result.thumbnailUrl = element["artworkUrl100"] as? String
        result.trackName = element["artistName"] as? String
        result.artistName = element["trackName"] as? String
        result.longDescription = element["longDescription"] as? String
        
        if  let amount = element["trackPrice"] as? Float,
            let currencyCode = element["currency"] as? String {
            result.price = ISPrice(amount: amount, currencyCode: currencyCode)
        }
        
        if let dateString = element["releaseDate"] as? String {
            let date = self.dateFormatter.date(from: dateString)
            result.releaseDate = date
        }
        
        return result
        
    }
    
    private func wrapperTypeFor(wrapperName: String) -> ISItunesItemWrapperType? {
        for type in iterateEnum(ISItunesItemWrapperType.self) {
            if type.rawValue == wrapperName {
                return type
            }
        }
        return nil
        
    }
    private func kindFor(kindName: String) -> ISItunesItemKind? {
        for kind in iterateEnum(ISItunesItemKind.self) {
            if kind.rawValue == kindName {
                return kind
            }
        }
        return nil
    }
}













