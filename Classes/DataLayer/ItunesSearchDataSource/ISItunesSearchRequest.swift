//
//  ISItunesSearchRequest.swift
//  itunesSearch-test
//
//  Created by Nicolas Yuste Tirados on 8/12/17.
//  Copyright © 2017 Nicolas Yuste. All rights reserved.
//

import Foundation

/// This enum lists all the media types that ISItunesSearchDataSource is able to search for on iTunes.
///
/// - MOVIE: Movies
/// - PODCAST: Podcasts
/// - MUSIC: This could be artists, albums, songs, etc...
/// - MUSIC_VIDEO: Video songs
/// - AUDIO_BOOK: An audiobooks
/// - SHORT_FILM: Short films
/// - TV_SHOW: TV shows and collection of tv shows
/// - SOFTWARE: Apps
/// - EBOOK: ebooks
/// - ALL: Any type of media will be retreive if we do all. This is the default value.
enum ISItunesSearchMediaType : String {
    case MOVIE = "movie"
    case PODCAST = "podcast"
    case MUSIC = "music"
    case MUSIC_VIDEO = "musicVideo"
    case AUDIO_BOOK = "audiobook"
    case SHORT_FILM = "shortFilm"
    case TV_SHOW = "tvShow"
    case SOFTWARE = "software"
    case EBOOK = "ebook"
    case ALL = "all"
}

/// An iTunes Search request that can be executed by ISItunesSearchDataSource
/// You can search different terms and different medias.
class ISItunesSearchRequest: NSObject, ISAPIClientRequest {
    
    private let term : String!
    private var mediaType : ISItunesSearchMediaType!
    
    // MARK: Init methods
    
    /// Creates and returns a new iTunesSearchRequest that can be executed by ISItunesSearchDataSource
    ///
    /// - Parameters:
    ///   - searchTerm: The text that we want to look for in itunes. For example: 'Jack Johnson'
    ///   - mediaType: The media type that we want to search for. It can be any type from ISItunesSearchMediaType
    init(searchTerm: String, mediaType: ISItunesSearchMediaType){
        self.term = searchTerm
        self.mediaType = mediaType
    }
    
    /// Creates and returns a new iTunesSearchRequest that can be executed by ISItunesSearchDataSource
    /// Media type is not be specified so it will use 'ALL' as the default value
    ///
    /// - Parameters:
    ///   - searchTerm: The text that we want to look for in itunes. For example: 'Jack Johnson'
    convenience init(searchTerm: String){
        self.init(searchTerm: searchTerm, mediaType: .ALL)
    }
    
    // MARK: - Implementing ISAPIClientRequest Protocol
    
    func getHost() -> String {
        return "itunes.apple.com"
    }
    func getMethod() -> ISAPIClientMethod {
        return .GET
    }
    func getPath() -> String {
        return "search"
    }
    func getBoby() -> Dictionary<String, Any>? {
        return nil
    }
    func getRequestHeaders() -> Dictionary<String, String>? {
        return nil
    }
    func getQueryParams() -> Dictionary<String, String>? {
        
        var params : Dictionary<String, String> = ["term": self.term]
        
        if let media = self.mediaType {
            params["media"] = media.rawValue
        }
        return params
    }
}
