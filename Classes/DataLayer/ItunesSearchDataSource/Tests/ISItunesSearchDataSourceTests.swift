//
//  ISItunesSearchDataSourceTests.swift
//  itunesSearch-test
//
//  Created by Nicolas Yuste Tirados on 8/12/17.
//  Copyright © 2017 Nicolas Yuste. All rights reserved.
//

import XCTest
@testable import itunesSearch_test

class ISItunesSearchDataSourceTests: XCTestCase {
    
    var semaphore : DispatchSemaphore? = nil
    let mockRequest = ISItunesSearchRequest(searchTerm: "search text")
    override func setUp() {
        self.semaphore = DispatchSemaphore(value: 0)
    }
    override func tearDown() {
        _ = self.semaphore!.wait(timeout: DispatchTime(uptimeNanoseconds: 1000))
    }
    
    func testDataSourceGoodResponse() {
        let dataSource = ISItunesSearchDataSource(apiClient: mockAPIClient(returnSuccess: true, jsonResponse:testGoodJSONResponse))
        dataSource.executeRequest(request: self.mockRequest) { (response: ISItunesSearchResponse) in
            XCTAssert(response.success, "Response success needs to be true since API is returning OK")
            XCTAssert(response.results?.count == 2, "Our test data has two results, there should be two results")
            self.semaphore?.signal()
        }
        
    }
    func testDataSourceFailResponse() {
        let dataSource = ISItunesSearchDataSource(apiClient: mockAPIClient(returnSuccess: false, jsonResponse: ""))
        dataSource.executeRequest(request: self.mockRequest) { (response: ISItunesSearchResponse) in
            XCTAssert(response.success == false, "Response success needs to be false since API is returning an error")
            self.semaphore?.signal()
        }
        
    }
    func testDataSourceFailWeirdGoodJSON() {
        let dataSource = ISItunesSearchDataSource(apiClient: mockAPIClient(returnSuccess: true, jsonResponse: testWeirdJSON))
        dataSource.executeRequest(request: self.mockRequest) { (response: ISItunesSearchResponse) in
            XCTAssert(response.success == false, "Response success needs to be false since API is returning an error")
            self.semaphore?.signal()
        }
        
    }
    func testDataSourceGoodResponseWithUnknownItemWrapper() {
        let dataSource = ISItunesSearchDataSource(apiClient: mockAPIClient(returnSuccess: true, jsonResponse: testGoodJSONResponseOneUnkownWrapper))
        dataSource.executeRequest(request: self.mockRequest) { (response: ISItunesSearchResponse) in
            XCTAssert(response.success, "Response success needs to be true since API is returning OK")
            XCTAssert(response.results?.count == 1, "Our test data has 1 result, second one has wrong wrapper")
            self.semaphore?.signal()
        }
    }
    func testDataSourceGoodResponseWithUnknownItemKind() {
        let dataSource = ISItunesSearchDataSource(apiClient: mockAPIClient(returnSuccess: true, jsonResponse: testGoodJSONResponseOneUnkownKind))
        dataSource.executeRequest(request: self.mockRequest) { (response: ISItunesSearchResponse) in
            XCTAssert(response.success, "Response success needs to be true since API is returning OK")
            XCTAssert(response.results?.count == 1, "Our test data has 1 result, second one has wrong kind")
            self.semaphore?.signal()
        }
    }
    func testDataSourceGoodResponseButErrorBecauseNoItemsWithRightWrapperOrKind() {
        let dataSource = ISItunesSearchDataSource(apiClient: mockAPIClient(returnSuccess: true, jsonResponse: testGoodJSONUnknownTypes))
        dataSource.executeRequest(request: self.mockRequest) { (response: ISItunesSearchResponse) in
            XCTAssert(response.success == false, "Response success needs to be false since API is returning an error")
            self.semaphore?.signal()
        }
    }
}


class mockAPIClient : NSObject, ISAPIClientProtocol {
    
    let returnSuccess:Bool!
    let jsonResponse:String!
    
    init(returnSuccess: Bool, jsonResponse:String) {
        self.returnSuccess = returnSuccess
        self.jsonResponse = jsonResponse
    }
    
    func executeRequest(request: ISAPIClientRequest, successBlock: @escaping ISAPISuccessClientCompletionBlock, failBlock: @escaping ISAPIFailureClientCompletionBlock) {
        
        if (returnSuccess) {
            if let data = self.jsonResponse.data(using: .utf8) {
                do {
                    let jsonDic = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
                    successBlock(jsonDic!)
                } catch {
                    XCTFail("We had a problem loading the JSON")
                }
            }
        } else {
            failBlock(ISCustomAPIClientError.invalidData)
        }
    }
    
}

let testGoodJSONResponse = "{\"resultCount\":2,\"results\":[{\"wrapperType\":\"track\",\"kind\":\"feature-movie\",\"collectionId\":414719158,\"trackId\":270784489,\"artistName\":\"Sam Raimi\",\"collectionName\":\"Spider Man Trilogy\",\"trackName\":\"Spider-Man\",\"collectionCensoredName\":\"Spider Man Trilogy\",\"trackCensoredName\":\"Spider-Man\",\"collectionArtistId\":345346702,\"collectionArtistViewUrl\":\"https://itunes.apple.com/us/artist/sony-pictures-entertainment/id345346702?uo=4\",\"collectionViewUrl\":\"https://itunes.apple.com/us/movie/spider-man/id270784489?uo=4\",\"trackViewUrl\":\"https://itunes.apple.com/us/movie/spider-man/id270784489?uo=4\",\"previewUrl\":\"http://video.itunes.apple.com/apple-assets-us-std-000001/Video/3d/6b/13/mzm.hswpwqua..640x450.h264lc.d2.p.m4v\",\"artworkUrl30\":\"http://is1.mzstatic.com/image/thumb/Video117/v4/26/0e/09/260e09f6-9251-3406-54b3-39f0cc7612d8/source/30x30bb.jpg\",\"artworkUrl60\":\"http://is1.mzstatic.com/image/thumb/Video117/v4/26/0e/09/260e09f6-9251-3406-54b3-39f0cc7612d8/source/60x60bb.jpg\",\"artworkUrl100\":\"http://is1.mzstatic.com/image/thumb/Video117/v4/26/0e/09/260e09f6-9251-3406-54b3-39f0cc7612d8/source/100x100bb.jpg\",\"collectionPrice\":9.99,\"trackPrice\":9.99,\"trackRentalPrice\":2.99,\"collectionHdPrice\":12.99,\"trackHdPrice\":12.99,\"trackHdRentalPrice\":3.99,\"releaseDate\":\"2002-05-03T07:00:00Z\",\"collectionExplicitness\":\"notExplicit\",\"trackExplicitness\":\"notExplicit\",\"discCount\":1,\"discNumber\":1,\"trackCount\":3,\"trackNumber\":1,\"trackTimeMillis\":7270862,\"country\":\"USA\",\"currency\":\"USD\",\"primaryGenreName\":\"Action & Adventure\",\"contentAdvisoryRating\":\"PG-13\",\"longDescription\":\"Average teenager Peter Parker is transformed into an extraordinary super hero after he is accidentally bitten by a radioactive spider. When his beloved uncle is savagely murdered during a robbery, young Peter vows to use his powers to avenge his death. Deeming himself Spider-Man, he sets about ridding the streets of crime, bringing him into conflict with malevolent super-villain Green Goblin.\",\"hasITunesExtras\":true},{\"wrapperType\":\"track\",\"kind\":\"tv-episode\",\"artistId\":219884400,\"collectionId\":219884393,\"trackId\":220126430,\"artistName\":\"Spider-Man\",\"collectionName\":\"Spider-Man (The New Animated Series), Season 1\",\"trackName\":\"Mind Games, Pt. 2\",\"collectionCensoredName\":\"Spider-Man (The New Animated Series), Season 1\",\"trackCensoredName\":\"Mind Games, Pt. 2\",\"artistViewUrl\":\"https://itunes.apple.com/us/tv-show/spider-man/id219884400?uo=4\",\"collectionViewUrl\":\"https://itunes.apple.com/us/tv-season/mind-games-pt-2/id219884393?i=220126430&uo=4\",\"trackViewUrl\":\"https://itunes.apple.com/us/tv-season/mind-games-pt-2/id219884393?i=220126430&uo=4\",\"previewUrl\":\"http://video.itunes.apple.com/apple-assets-us-std-000001/Video111/v4/1e/dc/80/1edc8083-3595-9584-6293-19dbad451a49/mzvf_6436078153540402761.640x480.h264lc.U.p.m4v\",\"artworkUrl30\":\"http://is3.mzstatic.com/image/thumb/Video/v4/73/db/f2/73dbf226-684b-eae5-e17d-3cb6d39e1ede/source/30x30bb.jpg\",\"artworkUrl60\":\"http://is3.mzstatic.com/image/thumb/Video/v4/73/db/f2/73dbf226-684b-eae5-e17d-3cb6d39e1ede/source/60x60bb.jpg\",\"artworkUrl100\":\"http://is3.mzstatic.com/image/thumb/Video/v4/73/db/f2/73dbf226-684b-eae5-e17d-3cb6d39e1ede/source/100x100bb.jpg\",\"collectionPrice\":19.99,\"trackPrice\":1.99,\"releaseDate\":\"2003-01-01T08:00:00Z\",\"collectionExplicitness\":\"notExplicit\",\"trackExplicitness\":\"notExplicit\",\"discCount\":1,\"discNumber\":1,\"trackCount\":13,\"trackNumber\":13,\"trackTimeMillis\":1295670,\"country\":\"USA\",\"currency\":\"USD\",\"primaryGenreName\":\"Kids\",\"shortDescription\":\"Spider-Man is hypnotized and harms Indy.\",\"longDescription\":\"Spider-Man (Neil Patrick Harris) is hypnotized by the mental powers of the Gaines Twins. After Spidey is tricked into harming Indy (Angelle Brooks) he throws away his suit and contemplates his future.\"}]}"

let testGoodJSONResponseOneUnkownKind = "{\"resultCount\":2,\"results\":[{\"wrapperType\":\"track\",\"kind\":\"UNKNONW_KIND\",\"collectionId\":414719158,\"trackId\":270784489,\"artistName\":\"Sam Raimi\",\"collectionName\":\"Spider Man Trilogy\",\"trackName\":\"Spider-Man\",\"collectionCensoredName\":\"Spider Man Trilogy\",\"trackCensoredName\":\"Spider-Man\",\"collectionArtistId\":345346702,\"collectionArtistViewUrl\":\"https://itunes.apple.com/us/artist/sony-pictures-entertainment/id345346702?uo=4\",\"collectionViewUrl\":\"https://itunes.apple.com/us/movie/spider-man/id270784489?uo=4\",\"trackViewUrl\":\"https://itunes.apple.com/us/movie/spider-man/id270784489?uo=4\",\"previewUrl\":\"http://video.itunes.apple.com/apple-assets-us-std-000001/Video/3d/6b/13/mzm.hswpwqua..640x450.h264lc.d2.p.m4v\",\"artworkUrl30\":\"http://is1.mzstatic.com/image/thumb/Video117/v4/26/0e/09/260e09f6-9251-3406-54b3-39f0cc7612d8/source/30x30bb.jpg\",\"artworkUrl60\":\"http://is1.mzstatic.com/image/thumb/Video117/v4/26/0e/09/260e09f6-9251-3406-54b3-39f0cc7612d8/source/60x60bb.jpg\",\"artworkUrl100\":\"http://is1.mzstatic.com/image/thumb/Video117/v4/26/0e/09/260e09f6-9251-3406-54b3-39f0cc7612d8/source/100x100bb.jpg\",\"collectionPrice\":9.99,\"trackPrice\":9.99,\"trackRentalPrice\":2.99,\"collectionHdPrice\":12.99,\"trackHdPrice\":12.99,\"trackHdRentalPrice\":3.99,\"releaseDate\":\"2002-05-03T07:00:00Z\",\"collectionExplicitness\":\"notExplicit\",\"trackExplicitness\":\"notExplicit\",\"discCount\":1,\"discNumber\":1,\"trackCount\":3,\"trackNumber\":1,\"trackTimeMillis\":7270862,\"country\":\"USA\",\"currency\":\"USD\",\"primaryGenreName\":\"Action & Adventure\",\"contentAdvisoryRating\":\"PG-13\",\"longDescription\":\"Average teenager Peter Parker is transformed into an extraordinary super hero after he is accidentally bitten by a radioactive spider. When his beloved uncle is savagely murdered during a robbery, young Peter vows to use his powers to avenge his death. Deeming himself Spider-Man, he sets about ridding the streets of crime, bringing him into conflict with malevolent super-villain Green Goblin.\",\"hasITunesExtras\":true},{\"wrapperType\":\"track\",\"kind\":\"tv-episode\",\"artistId\":219884400,\"collectionId\":219884393,\"trackId\":220126430,\"artistName\":\"Spider-Man\",\"collectionName\":\"Spider-Man (The New Animated Series), Season 1\",\"trackName\":\"Mind Games, Pt. 2\",\"collectionCensoredName\":\"Spider-Man (The New Animated Series), Season 1\",\"trackCensoredName\":\"Mind Games, Pt. 2\",\"artistViewUrl\":\"https://itunes.apple.com/us/tv-show/spider-man/id219884400?uo=4\",\"collectionViewUrl\":\"https://itunes.apple.com/us/tv-season/mind-games-pt-2/id219884393?i=220126430&uo=4\",\"trackViewUrl\":\"https://itunes.apple.com/us/tv-season/mind-games-pt-2/id219884393?i=220126430&uo=4\",\"previewUrl\":\"http://video.itunes.apple.com/apple-assets-us-std-000001/Video111/v4/1e/dc/80/1edc8083-3595-9584-6293-19dbad451a49/mzvf_6436078153540402761.640x480.h264lc.U.p.m4v\",\"artworkUrl30\":\"http://is3.mzstatic.com/image/thumb/Video/v4/73/db/f2/73dbf226-684b-eae5-e17d-3cb6d39e1ede/source/30x30bb.jpg\",\"artworkUrl60\":\"http://is3.mzstatic.com/image/thumb/Video/v4/73/db/f2/73dbf226-684b-eae5-e17d-3cb6d39e1ede/source/60x60bb.jpg\",\"artworkUrl100\":\"http://is3.mzstatic.com/image/thumb/Video/v4/73/db/f2/73dbf226-684b-eae5-e17d-3cb6d39e1ede/source/100x100bb.jpg\",\"collectionPrice\":19.99,\"trackPrice\":1.99,\"releaseDate\":\"2003-01-01T08:00:00Z\",\"collectionExplicitness\":\"notExplicit\",\"trackExplicitness\":\"notExplicit\",\"discCount\":1,\"discNumber\":1,\"trackCount\":13,\"trackNumber\":13,\"trackTimeMillis\":1295670,\"country\":\"USA\",\"currency\":\"USD\",\"primaryGenreName\":\"Kids\",\"shortDescription\":\"Spider-Man is hypnotized and harms Indy.\",\"longDescription\":\"Spider-Man (Neil Patrick Harris) is hypnotized by the mental powers of the Gaines Twins. After Spidey is tricked into harming Indy (Angelle Brooks) he throws away his suit and contemplates his future.\"}]}"

let testGoodJSONUnknownTypes = "{\"resultCount\":2,\"results\":[{\"wrapperType\":\"I_DONT_KNOW_THIS_WRAPPER\",\"kind\":\"I_DONT_KNOW_THIS_KIND\",\"artistId\":219884400,\"collectionId\":219884393,\"trackId\":220126430,\"artistName\":\"Spider-Man\",\"collectionName\":\"Spider-Man (The New Animated Series), Season 1\",\"trackName\":\"Mind Games, Pt. 2\",\"collectionCensoredName\":\"Spider-Man (The New Animated Series), Season 1\",\"trackCensoredName\":\"Mind Games, Pt. 2\",\"artistViewUrl\":\"https://itunes.apple.com/us/tv-show/spider-man/id219884400?uo=4\",\"collectionViewUrl\":\"https://itunes.apple.com/us/tv-season/mind-games-pt-2/id219884393?i=220126430&uo=4\",\"trackViewUrl\":\"https://itunes.apple.com/us/tv-season/mind-games-pt-2/id219884393?i=220126430&uo=4\",\"previewUrl\":\"http://video.itunes.apple.com/apple-assets-us-std-000001/Video111/v4/1e/dc/80/1edc8083-3595-9584-6293-19dbad451a49/mzvf_6436078153540402761.640x480.h264lc.U.p.m4v\",\"artworkUrl30\":\"http://is3.mzstatic.com/image/thumb/Video/v4/73/db/f2/73dbf226-684b-eae5-e17d-3cb6d39e1ede/source/30x30bb.jpg\",\"artworkUrl60\":\"http://is3.mzstatic.com/image/thumb/Video/v4/73/db/f2/73dbf226-684b-eae5-e17d-3cb6d39e1ede/source/60x60bb.jpg\",\"artworkUrl100\":\"http://is3.mzstatic.com/image/thumb/Video/v4/73/db/f2/73dbf226-684b-eae5-e17d-3cb6d39e1ede/source/100x100bb.jpg\",\"collectionPrice\":19.99,\"trackPrice\":1.99,\"releaseDate\":\"2003-01-01T08:00:00Z\",\"collectionExplicitness\":\"notExplicit\",\"trackExplicitness\":\"notExplicit\",\"discCount\":1,\"discNumber\":1,\"trackCount\":13,\"trackNumber\":13,\"trackTimeMillis\":1295670,\"country\":\"USA\",\"currency\":\"USD\",\"primaryGenreName\":\"Kids\",\"shortDescription\":\"Spider-Man is hypnotized and harms Indy.\",\"longDescription\":\"Spider-Man (Neil Patrick Harris) is hypnotized by the mental powers of the Gaines Twins. After Spidey is tricked into harming Indy (Angelle Brooks) he throws away his suit and contemplates his future.\"}]}"

let testWeirdJSON = "{\"data\":{\"NO_DATA\":\"NO_DATA\"}}"

let testGoodJSONResponseOneUnkownWrapper = "{\"resultCount\":2,\"results\":[{\"wrapperType\":\"MI_UNKKNOWN_WRAPPER\",\"kind\":\"feature-movie\",\"collectionId\":414719158,\"trackId\":270784489,\"artistName\":\"Sam Raimi\",\"collectionName\":\"Spider Man Trilogy\",\"trackName\":\"Spider-Man\",\"collectionCensoredName\":\"Spider Man Trilogy\",\"trackCensoredName\":\"Spider-Man\",\"collectionArtistId\":345346702,\"collectionArtistViewUrl\":\"https://itunes.apple.com/us/artist/sony-pictures-entertainment/id345346702?uo=4\",\"collectionViewUrl\":\"https://itunes.apple.com/us/movie/spider-man/id270784489?uo=4\",\"trackViewUrl\":\"https://itunes.apple.com/us/movie/spider-man/id270784489?uo=4\",\"previewUrl\":\"http://video.itunes.apple.com/apple-assets-us-std-000001/Video/3d/6b/13/mzm.hswpwqua..640x450.h264lc.d2.p.m4v\",\"artworkUrl30\":\"http://is1.mzstatic.com/image/thumb/Video117/v4/26/0e/09/260e09f6-9251-3406-54b3-39f0cc7612d8/source/30x30bb.jpg\",\"artworkUrl60\":\"http://is1.mzstatic.com/image/thumb/Video117/v4/26/0e/09/260e09f6-9251-3406-54b3-39f0cc7612d8/source/60x60bb.jpg\",\"artworkUrl100\":\"http://is1.mzstatic.com/image/thumb/Video117/v4/26/0e/09/260e09f6-9251-3406-54b3-39f0cc7612d8/source/100x100bb.jpg\",\"collectionPrice\":9.99,\"trackPrice\":9.99,\"trackRentalPrice\":2.99,\"collectionHdPrice\":12.99,\"trackHdPrice\":12.99,\"trackHdRentalPrice\":3.99,\"releaseDate\":\"2002-05-03T07:00:00Z\",\"collectionExplicitness\":\"notExplicit\",\"trackExplicitness\":\"notExplicit\",\"discCount\":1,\"discNumber\":1,\"trackCount\":3,\"trackNumber\":1,\"trackTimeMillis\":7270862,\"country\":\"USA\",\"currency\":\"USD\",\"primaryGenreName\":\"Action & Adventure\",\"contentAdvisoryRating\":\"PG-13\",\"longDescription\":\"Average teenager Peter Parker is transformed into an extraordinary super hero after he is accidentally bitten by a radioactive spider. When his beloved uncle is savagely murdered during a robbery, young Peter vows to use his powers to avenge his death. Deeming himself Spider-Man, he sets about ridding the streets of crime, bringing him into conflict with malevolent super-villain Green Goblin.\",\"hasITunesExtras\":true},{\"wrapperType\":\"track\",\"kind\":\"tv-episode\",\"artistId\":219884400,\"collectionId\":219884393,\"trackId\":220126430,\"artistName\":\"Spider-Man\",\"collectionName\":\"Spider-Man (The New Animated Series), Season 1\",\"trackName\":\"Mind Games, Pt. 2\",\"collectionCensoredName\":\"Spider-Man (The New Animated Series), Season 1\",\"trackCensoredName\":\"Mind Games, Pt. 2\",\"artistViewUrl\":\"https://itunes.apple.com/us/tv-show/spider-man/id219884400?uo=4\",\"collectionViewUrl\":\"https://itunes.apple.com/us/tv-season/mind-games-pt-2/id219884393?i=220126430&uo=4\",\"trackViewUrl\":\"https://itunes.apple.com/us/tv-season/mind-games-pt-2/id219884393?i=220126430&uo=4\",\"previewUrl\":\"http://video.itunes.apple.com/apple-assets-us-std-000001/Video111/v4/1e/dc/80/1edc8083-3595-9584-6293-19dbad451a49/mzvf_6436078153540402761.640x480.h264lc.U.p.m4v\",\"artworkUrl30\":\"http://is3.mzstatic.com/image/thumb/Video/v4/73/db/f2/73dbf226-684b-eae5-e17d-3cb6d39e1ede/source/30x30bb.jpg\",\"artworkUrl60\":\"http://is3.mzstatic.com/image/thumb/Video/v4/73/db/f2/73dbf226-684b-eae5-e17d-3cb6d39e1ede/source/60x60bb.jpg\",\"artworkUrl100\":\"http://is3.mzstatic.com/image/thumb/Video/v4/73/db/f2/73dbf226-684b-eae5-e17d-3cb6d39e1ede/source/100x100bb.jpg\",\"collectionPrice\":19.99,\"trackPrice\":1.99,\"releaseDate\":\"2003-01-01T08:00:00Z\",\"collectionExplicitness\":\"notExplicit\",\"trackExplicitness\":\"notExplicit\",\"discCount\":1,\"discNumber\":1,\"trackCount\":13,\"trackNumber\":13,\"trackTimeMillis\":1295670,\"country\":\"USA\",\"currency\":\"USD\",\"primaryGenreName\":\"Kids\",\"shortDescription\":\"Spider-Man is hypnotized and harms Indy.\",\"longDescription\":\"Spider-Man (Neil Patrick Harris) is hypnotized by the mental powers of the Gaines Twins. After Spidey is tricked into harming Indy (Angelle Brooks) he throws away his suit and contemplates his future.\"}]}"
