//
//  ISAPIClient.swift
//  itunesSearch-test
//
//  Created by Nicolas Yuste Tirados on 8/11/17.
//  Copyright © 2017 Nicolas Yuste. All rights reserved.
//

import Foundation

/// Defines all the possible HTTP Methods that this API supports.
/// TODO: Add more methods than GET, we only needed this for now.
enum ISAPIClientMethod : String {
    case GET = "GET"
}

/// Completion block that ISAPIClient fires when it finishs executing a request and it is succcesful.
typealias ISAPISuccessClientCompletionBlock = ([String:Any]) -> Void

/// Completion block that ISAPIClient fires when it finishs executing a request and it fails.
typealias ISAPIFailureClientCompletionBlock = (Error) -> Void


/// A protocol that defines how an APIClient should behave.
/// This way, if I ever want to create a new API, I can easily change the implementation and keep the same interface.
protocol ISAPIClientProtocol {
    func executeRequest(request: ISAPIClientRequest, successBlock: @escaping ISAPISuccessClientCompletionBlock, failBlock: @escaping ISAPIFailureClientCompletionBlock);
}

/// An API Client that is able to execute any type of JSON API request.
/// It will return the JSON object as a Dictionary.
final class ISAPIClient: NSObject, ISAPIClientProtocol {
    
    // Single instand of the API
    public static let shared = ISAPIClient()
    
    //This prevents others from using the default '()' initializer for this class.
    private override init() {}
    
    // We keep this as a variable so it is easier to test this class. Check ISAPIClientTests
    var session: URLSession = URLSession.shared
        
    /// Asynchronous executes an ISAPIClientRequest request.
    ///
    /// - Parameters:
    ///   - request: The request that is going to be executed.
    ///   - successBlock: The success block that will be executed in case this class completes the API request successfully.
    ///   - failBlock: The error block that will be executed in case this class completes the API request with an error.
    func executeRequest(request: ISAPIClientRequest, successBlock: @escaping ISAPISuccessClientCompletionBlock, failBlock: @escaping ISAPIFailureClientCompletionBlock) {
        
        guard let urlRequest = self.getURLRequestFrom(apiRequest: request) else {
            self.executeFailBlock(failBlock: failBlock, error: ISCustomAPIClientError.invalidUrl)
            return
        }
        
        let task = self.session.dataTask(with: urlRequest) { (data: Data?, response: URLResponse?, error: Error?) in
            
            if let error = error {
                self.executeFailBlock(failBlock: failBlock, error: error)
                return
            }
            // make sure we got data
            guard let responseData = data else {
                self.executeFailBlock(failBlock: failBlock, error: ISCustomAPIClientError.invalidData)
                return
            }
            // parse the result as JSON, since that's what the API provides
            guard let jsonResponse = try? JSONSerialization.jsonObject(with: responseData, options: []),
                  let result = jsonResponse as? [String: Any] else {
                self.executeFailBlock(failBlock: failBlock, error: ISCustomAPIClientError.invalidJson)
                return
            }
            
            DispatchQueue.main.async {
                successBlock(result)
            }
            
        }
        
        task.resume()
    }
    
    // MARK: - Internal helpers
    
    private func executeFailBlock(failBlock:@escaping ISAPIFailureClientCompletionBlock, error: Error){
         DispatchQueue.main.async {
            failBlock(error)
        }
    }
    
    private func getURLRequestFrom(apiRequest: ISAPIClientRequest) -> URLRequest? {
        
        var endpoint: String =  String(format: "https://%@/%@", apiRequest.getHost(), apiRequest.getPath() )
        
        if let queryParams = apiRequest.getQueryParams() {
            var components = URLComponents()
            components.queryItems = queryParams.map {
                URLQueryItem(name: $0, value: $1)
            }
            if let queryString = components.url?.absoluteString {
                endpoint += queryString
            }
        }

        // Creating the URLRequest with the right endpoint and params
        guard let endpointURL = URL(string: endpoint) else {
            return nil
        }
        var urlRequest = URLRequest(url: endpointURL)
        
        // Adding the right HTTP Method to the request
        urlRequest.httpMethod = apiRequest.getMethod().rawValue
        
        // Adding the headers in case there is any.
        if let headers = apiRequest.getRequestHeaders() {
            for (header, value) in headers {
                urlRequest.addValue(value, forHTTPHeaderField: header)
            }
        }
        
        // Adding the body to the request in case there is any
        if let body = apiRequest.getBoby() {
            let jsonData = try? JSONSerialization.data(withJSONObject: body)
            urlRequest.httpBody = jsonData
        }
        
        return urlRequest
    }
    
}

enum ISCustomAPIClientError: Error {
    case invalidUrl
    case invalidData
    case invalidJson
    case outOfStock
}

