//
//  ISAPIClientTests.swift
//  itunesSearch-test
//
//  Created by Nicolas Yuste Tirados on 8/12/17.
//  Copyright © 2017 Nicolas Yuste. All rights reserved.
//

import XCTest
import Foundation
@testable import itunesSearch_test

class ISAPIClientTests: XCTestCase {
    
    var semaphore : DispatchSemaphore? = nil
    
    override func setUp() {
        self.semaphore = DispatchSemaphore(value: 0)
    }
    override func tearDown() {
        _ = self.semaphore!.wait(timeout: DispatchTime(uptimeNanoseconds: 1000))
        NotificationCenter.default.removeObserver(self)
    }
    
    func testURLSessionReturnsValidJson(){
        
        let mockDataResponse = try? JSONSerialization.data(withJSONObject: ["data":"response"])
        let mockSession = mockURLSession(mockData: mockDataResponse, mockResponse: nil, mockError: nil)
        ISAPIClient.shared.session = mockSession
        
        let mockRequest = mockAPIRequest(method: .GET, host: "host", path: "path", query: nil, headers: nil, body: nil)
        
        ISAPIClient.shared.executeRequest(request: mockRequest, successBlock: { (response: Dictionary<String, Any>) in
            XCTAssert(response["data"] as! String == "response", "JSON object that we get back is not the expected")
            self.semaphore?.signal()
        }) { (error: Error) in
            XCTFail("We should not be here, we are testing a good JSON")
            self.semaphore?.signal()
        }
    }
    func testURLSessionReturnsInvalidData(){
        
        let mockDataResponse = Data(capacity: 32)
        let mockSession = mockURLSession(mockData: mockDataResponse, mockResponse: nil, mockError: nil)
        ISAPIClient.shared.session = mockSession
        
        let mockRequest = mockAPIRequest(method: .GET, host: "host", path: "path", query: nil, headers: nil, body: nil)
        
        ISAPIClient.shared.executeRequest(request: mockRequest, successBlock: { (response: Dictionary<String, Any>) in
            XCTFail("We should not be here, we are testing invalid data")
            self.semaphore?.signal()
        }) { (error: Error) in
            self.semaphore?.signal()
        }
    }
    func testURLSessionReturnsError(){
        
        let mockSession = mockURLSession(mockData: nil, mockResponse: nil, mockError: ISCustomAPIClientError.invalidData)
        ISAPIClient.shared.session = mockSession
        
        let mockRequest = mockAPIRequest(method: .GET, host: "host", path: "path", query: nil, headers: nil, body: nil)
        
        ISAPIClient.shared.executeRequest(request: mockRequest, successBlock: { (response: Dictionary<String, Any>) in
            XCTFail("We should not be here, we are testing an error Response")
            self.semaphore?.signal()
        }) { (error: Error) in
            self.semaphore?.signal()
        }
    }
    func testAPIRequestfields(){
        let mockSession = mockURLSession(mockData: nil, mockResponse: nil, mockError: nil)
        ISAPIClient.shared.session = mockSession
        let mockRequest = mockAPIRequest(method: .GET, host: "host", path: "path", query: ["query":"value"], headers: ["header":"value"], body: ["body":"value"])
        NotificationCenter.default.addObserver(self, selector: #selector(validateURLRequest), name: MockSessionReceivedURLRequest.name, object: nil)
        
        ISAPIClient.shared.executeRequest(request: mockRequest, successBlock: { (response: Dictionary<String, Any>) in}) { (error: Error) in }
    }
    func testAPIRequestQueryCanHaveMoreThanOneWord(){
        let mockSession = mockURLSession(mockData: nil, mockResponse: nil, mockError: nil)
        ISAPIClient.shared.session = mockSession
        let mockRequest = mockAPIRequest(method: .GET, host: "host", path: "path", query: ["query":"value with space"], headers: ["header":"value"], body: ["body":"value"])
        NotificationCenter.default.addObserver(self, selector: #selector(validateURLRequest), name: MockSessionReceivedURLRequest.name, object: nil)
        
        ISAPIClient.shared.executeRequest(request: mockRequest, successBlock: { (response: Dictionary<String, Any>) in}) { (error: Error) in }
    
    }
    
    func validateURLRequest(notification: Notification) {
        
        if notification.name == MockSessionReceivedURLRequest.name {
            let urlRequest = notification.userInfo?["request"] as! URLRequest
        
            XCTAssert((urlRequest.url?.absoluteString.contains("https://host/path?query=value"))!, "URL is not built correctly")
            XCTAssert(urlRequest.httpMethod == "GET", "URL Method is not correct")
            XCTAssert((urlRequest.allHTTPHeaderFields?.description.contains("\"header\": \"value\""))!, "Headers do not contain whatever we sent in API Request")
            
            do {
                let jsonResponse = try JSONSerialization.jsonObject(with: urlRequest.httpBody!, options: []) as? [String:String]
                XCTAssert((jsonResponse?.description.contains("\"body\": \"value\""))!, "Headers do not contain whatever we sent in API Request")
            } catch { XCTFail("Errow with JSON") }
            
        
            self.semaphore?.signal()
        }
    }
}

//
// MARK: Mocking URLSession in order to be able to test the API Client
//

class mockAPIRequest: ISAPIClientRequest {
    
    var host: String!
    var path: String!
    var method: ISAPIClientMethod!
    var query: Dictionary<String, String>?
    var headers: Dictionary<String, String>?
    var body: Dictionary<String, Any>?
    
    init(method: ISAPIClientMethod, host: String, path: String, query: Dictionary<String, String>?, headers: Dictionary<String, String>?, body: Dictionary<String, Any>? ){
        self.host = host
        self.path = path
        self.method = method
        self.query = query
        self.body = body
        self.headers = headers
    }
    
    // MARK: Implement API Protocol
    
    func getHost() -> String {
        return self.host
    }
    func getMethod() -> ISAPIClientMethod {
        return self.method
    }
    func getPath() -> String {
        return self.path
    }
    func getBoby() -> Dictionary<String, Any>? {
        return self.body
    }
    func getRequestHeaders() -> Dictionary<String, String>? {
        return self.headers
    }
    func getQueryParams() -> Dictionary<String, String>? {
        return self.query
    }
    
}

class mockURLSession : URLSession {
    
    var completionHandler : ((Data?, URLResponse?, Error?) -> Void)?
    
    var mockData: Data?
    var mockResponse: URLResponse?
    var mockError: Error?
    
    init(mockData: Data?, mockResponse: URLResponse?, mockError: Error?) {
        super.init()
        self.mockData = mockData
        self.mockResponse = mockResponse
        self.mockError = mockError
        NotificationCenter.default.addObserver(self, selector: #selector(receivedTaskResumeNotification), name: MockURLSessionDataTaskNotification.name, object: nil)
    }
    override func dataTask(with request: URLRequest, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        self.completionHandler = completionHandler
        NotificationCenter.default.post(name: MockSessionReceivedURLRequest.name, object: nil, userInfo: ["request":request])
        return MockURLSessionDataTask()
    }
    func receivedTaskResumeNotification() {
        if let completion = self.completionHandler {
            completion(self.mockData,self.mockResponse,self.mockError)
        }
    }
    
}

class MockURLSessionDataTask : URLSessionDataTask {
    override func resume() {
        NotificationCenter.default.post(MockURLSessionDataTaskNotification)
    }
}

let MockSessionReceivedURLRequest : Notification = Notification(name: Notification.Name(rawValue:"mockSession.didReceive.request"))
let MockURLSessionDataTaskNotification : Notification = Notification(name: Notification.Name(rawValue:"mockTask.called.resume"))

