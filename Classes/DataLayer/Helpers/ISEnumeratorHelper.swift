//
//  ISEnumeratorHelper.swift
//  itunesSearch-test
//
//  Created by Nicolas Yuste Tirados on 8/12/17.
//  Copyright © 2017 Nicolas Yuste. All rights reserved.
//

import Foundation

/// This function is able to enumerate through all the elements of an enum
/// You can see an example of how to use in ISItunesSearchDataSource class.
///
/// - Parameter _: The enum that we want to enumerate through
/// - Returns: An iterator with all the elements in the enum
func iterateEnum<T: Hashable>(_: T.Type) -> AnyIterator<T> {
    var i = 0
    return AnyIterator {
        let next = withUnsafeBytes(of: &i) { $0.load(as: T.self) }
        if next.hashValue != i { return nil }
        i += 1
        return next
    }
}
